package be.xl.shop.catalog.projection;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import be.xl.shop.catalog.event.ProductDefined;
import be.xl.shop.catalog.event.ProductDescriptionChanged;
import be.xl.shop.catalog.event.ProductNameChanged;
import be.xl.shop.catalog.model.Product;
import be.xl.shop.catalog.model.ProductEntity;
import be.xl.shop.catalog.model.ProductId;
import be.xl.shop.catalog.model.ProductOwner;
import be.xl.shop.catalog.model.Tenant;
import be.xl.test.FakeStateStoreDispatcher;
import io.vlingo.actors.Configuration;
import io.vlingo.actors.Definition;
import io.vlingo.actors.GridAddressFactory;
import io.vlingo.actors.Protocols;
import io.vlingo.actors.Protocols.Two;
import io.vlingo.actors.Stage;
import io.vlingo.actors.World;
import io.vlingo.actors.testkit.TestUntil;
import io.vlingo.common.Tuple2;
import io.vlingo.common.identity.IdentityGeneratorType;
import io.vlingo.lattice.model.DomainEvent;
import io.vlingo.lattice.model.projection.ProjectionDispatcher;
import io.vlingo.lattice.model.projection.ProjectionDispatcher.ProjectToDescription;
import io.vlingo.lattice.model.projection.TextProjectionDispatcherActor;
import io.vlingo.lattice.model.sourcing.SourcedTypeRegistry;
import io.vlingo.lattice.model.stateful.StatefulTypeRegistry;
import io.vlingo.symbio.Entry;
import io.vlingo.symbio.State.TextState;
import io.vlingo.symbio.store.dispatch.Dispatchable;
import io.vlingo.symbio.store.dispatch.Dispatcher;
import io.vlingo.symbio.store.journal.Journal;
import io.vlingo.symbio.store.journal.inmemory.InMemoryJournalActor;
import io.vlingo.symbio.store.state.StateStore;
import io.vlingo.symbio.store.state.StateTypeStateStoreMap;
import io.vlingo.symbio.store.state.inmemory.InMemoryStateStoreActor;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductProjectionActorTest {
  private World world;
  private StateStore stateStore;
  private FakeStateStoreDispatcher storeDispatcher;
  private Journal<DomainEvent> journal;

  @BeforeEach
  void setup() {
    world =
        World.start(
            "product",
            Configuration.define().with(new GridAddressFactory(IdentityGeneratorType.RANDOM)));
    storeDispatcher = new FakeStateStoreDispatcher();
    stateStore = createStateStore(world, storeDispatcher);
    ProjectionDispatcherProvider projectionDispatcherProvider =
        ProjectionDispatcherProvider.using(world.stageNamed("product"), stateStore);

    journal = createJournal(world, stateStore, projectionDispatcherProvider);
  }

  @AfterEach
  void tearDown() {
    world.terminate();
  }

  @Test
  void test2() {
    Tenant tenant = Tenant.with(UUID.randomUUID().toString());
    ProductOwner productOwner = ProductOwner.with(tenant, UUID.randomUUID().toString());
    Tuple2<ProductId, Product> productIdProduct =
        Product.defineWith(world.stage(), tenant, productOwner, "MyProduct", "A great product");
    productIdProduct._2.changeDescription("My other description");
    productIdProduct._2.changeName("My other name");

    Stage productStage = world.stageNamed("product");
  }

  @Test
  void test() {
    System.out.println("My test is running!");

    Tenant tenant = Tenant.with(UUID.randomUUID().toString());
    ProductOwner productOwner = ProductOwner.with(tenant, UUID.randomUUID().toString());

    Tuple2<ProductId, Product> productIdProduct =
        Product.defineWith(world.stage(), tenant, productOwner, "MyProduct", "A great product");
    productIdProduct._2.changeDescription("My other description");
    productIdProduct._2.changeName("My other name");

    waitForEvents(3, aVoid -> {});
  }

  private StateStore createStateStore(
      World world, Dispatcher<Dispatchable<Entry<String>, TextState>> dispatcher) {
    StateTypeStateStoreMap.stateTypeToStoreName(
        ProductView.class, ProductView.class.getSimpleName());

    StateStore stateStore =
        world.actorFor(
            StateStore.class, InMemoryStateStoreActor.class, singletonList(dispatcher));
    StatefulTypeRegistry registry = new StatefulTypeRegistry(world);
    registry.register(
        new StatefulTypeRegistry.Info(
            stateStore, ProductView.class, ProductView.class.getSimpleName()));
    return stateStore;
  }

  private Journal<DomainEvent> createJournal(
      World world, StateStore store, ProjectionDispatcherProvider projectionDispatcherProvider) {

    ProjectToDescription projectToDescription =
        ProjectToDescription.with(
            ProductProjectionActor.class,
            Optional.of(store),
            ProductDefined.class,
            ProductNameChanged.class,
            ProductDescriptionChanged.class);

    List<Object> descriptions = singletonList(projectToDescription);
    Protocols dispatcherProtocols =
        world.actorFor(
            new Class[] {Dispatcher.class, ProjectionDispatcher.class},
            Definition.has(
                TextProjectionDispatcherActor.class, singletonList(descriptions)));

    Two<Dispatcher, ProjectionDispatcher> dispatchers = Protocols.two(dispatcherProtocols);

    Journal journal =
        Journal.using(world.stage(), InMemoryJournalActor.class, dispatchers._1);
    SourcedTypeRegistry registry = new SourcedTypeRegistry(world);
    registry.register(
        new SourcedTypeRegistry.Info(
            journal, ProductEntity.class, ProductEntity.class.getSimpleName()));
    return journal;
  }

  private void waitForEvents(int number, Consumer<Void> block) {
    storeDispatcher.updateExpectedEventHappenings(number);
    TestUntil.happenings(number);
    block.accept(null);
    assertThat(storeDispatcher.states()).hasSize(number);
  }
}
