package be.xl.test;

import io.vlingo.actors.testkit.AccessSafely;
import io.vlingo.symbio.Entry;
import io.vlingo.symbio.State;
import io.vlingo.symbio.State.TextState;
import io.vlingo.symbio.store.dispatch.Dispatchable;
import io.vlingo.symbio.store.dispatch.Dispatcher;
import io.vlingo.symbio.store.dispatch.DispatcherControl;
import java.util.ArrayList;
import java.util.List;

public class FakeStateStoreDispatcher
    implements Dispatcher<Dispatchable<Entry<String>, TextState>> {
  private List<State.TextState> states = new ArrayList<>();
  private DispatcherControl control;
  private AccessSafely access;

  public FakeStateStoreDispatcher() {
    access =
        AccessSafely.immediately()
            .writingWith("registerState", (State.TextState event) -> states.add(event))
            .readingWith("states", (o -> states));
  }

  @Override
  public void controlWith(DispatcherControl control) {
    this.control = control;
  }

  @Override
  public void dispatch(Dispatchable<Entry<String>, State.TextState> dispatchable) {
    dispatchable.state().ifPresent(s -> access.writeUsing("registerState", s));

    if (this.control != null) {
      this.control.confirmDispatched(dispatchable.id(), (result, dispatchId) -> {});
      this.control.confirmDispatched(dispatchable.id(), (result, dispatchId) -> {});
    }
  }

  public void updateExpectedEventHappenings(Integer times) {
    access = access.resetAfterCompletingTo(times);
  }

  public List<TextState> states() {
    return access.readFrom("states");
  }
}
