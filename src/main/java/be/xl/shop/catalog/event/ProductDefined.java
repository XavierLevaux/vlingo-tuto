package be.xl.shop.catalog.event;

import be.xl.shop.catalog.model.ProductId;
import be.xl.shop.catalog.model.ProductOwner;
import be.xl.shop.catalog.model.Tenant;

public class ProductDefined extends ProductEvent {
   public final String productOwnerId;
   public final String name;
   public final String description;

   public static ProductDefined productDefined(final Tenant tenant, final ProductId productId, final ProductOwner productOwner, final String name, final String description) {
      return new ProductDefined(tenant, productId, productOwner, name, description);
   }

   public ProductDefined(final Tenant tenant, final ProductId productId, final ProductOwner productOwner, final String name, final String description) {
      super(productId.id, tenant.id);
      this.productOwnerId = productOwner.id;
      this.name = name;
      this.description = description;
   }
}
