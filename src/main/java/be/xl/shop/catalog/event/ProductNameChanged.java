package be.xl.shop.catalog.event;

import be.xl.shop.catalog.model.ProductId;
import be.xl.shop.catalog.model.Tenant;

public class ProductNameChanged extends ProductEvent {

   public final String name;

   public static ProductNameChanged productNameChanged(final Tenant tenant, final ProductId productId, final String name) {
      return new ProductNameChanged(tenant, productId, name);
   }

   public ProductNameChanged(final Tenant tenant, final ProductId productId, final String name) {
      super(productId.id, tenant.id);
      this.name = name;
   }
}
