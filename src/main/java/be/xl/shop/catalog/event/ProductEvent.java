package be.xl.shop.catalog.event;

import io.vlingo.lattice.model.DomainEvent;

public abstract class ProductEvent extends DomainEvent {
  public final String productId;
  public final String tenantId;

  protected ProductEvent(final String productId, final String tenantId) {
    this.productId = productId;
    this.tenantId = tenantId;
  }

}
