package be.xl.shop.catalog.model;

import java.util.Objects;
import java.util.UUID;

public class ProductId {
   public final String id;

   private ProductId(final String id) {
      this.id = id;
   }

   public static ProductId fromExisting(final String referencedId) {
      return new ProductId(referencedId);
   }

   public static ProductId generateNew() {
      return new ProductId(UUID.randomUUID().toString());
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ProductId productId = (ProductId) o;
      return id.equals(productId.id);
   }

   @Override
   public int hashCode() {
      return Objects.hashCode(id);
   }

   @Override
   public String toString() {
      return id;
   }
}